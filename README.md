<!-- https://medium.com/@hakluke/haklukes-ultimate-oscp-guide-part-3-practical-hacking-tips-and-tricks-c38486f5fc97 -->
<!-- https://medium.com/@falconspy/oscp-approved-tools-b2b4e889e707 -->


***Main repo moved to:*** https://dev.sect0uch.world/SecT0uch/Cheatsheet-OSCP

# Cheatsheet-OSCP

## :no_entry: Forbidden tools :no_entry:

**You cannot use any of the following on the exam**

* Spoofing (IP, ARP, DNS, NBNS, etc) 
* Commercial tools or services (Metasploit Pro, Burp Pro, etc.)
* Automatic exploitation tools (e.g. db_autopwn, browser_autopwn, SQLmap, SQLninja etc.)
* Mass vulnerability scanners (e.g. Nessus, NeXpose, OpenVAS, Canvas, Core Impact, SAINT, etc.)
* Features in other tools that utilize either forbidden or restricted exam limitations


## Nmap

First of all, we need to know what boxes exist on the network nmap run a ping scan:

`nmap -sn 10.0.0.0/24`


Once I have chosen a host, the first thing I always do is:

`export targetip=X.X.X.X`

`nmap -A -oA nmap $targetip`

This will scan the 1024 most common ports, run OS detection, run default nmap scripts, and save the results in a number of formats in the current directory.

Scanning more deeply:

`nmap -v -p 0-65535 -A $targetip`

This will scan all 65535 ports on $targetip with a full connect scan. This scan will probably take a very long time. The -v stands for verbose, so that when a new port is discovered, it will print it out straight away instead of having to wait until the end of the scan, scanning this many ports over the internet takes a long time. I would often leave the scan running overnight, or move on to a different box in the meantime.

## HTTP/HTTPS

### Discovery/Fuzzing

The following tools can be used for web fuzzing:
- [FFuF](https://github.com/ffuf/ffuf) : Very similar to wfuzz and gobuster but much faster (My favorite at the moment).
- [meg](https://github.com/tomnomnom/meg) : Good to avoid flooding with traffic. Needs multiple hosts to be worthy, very slow otherwise.
- [wfuzz](https://github.com/xmendez/wfuzz) : 2nd choice
- [gobuster](https://github.com/OJ/gobuster) : 3rd choice
- [dirsearch](https://github.com/maurosoria/dirsearch) : Over complicated
- [TIDoS-Framework](https://github.com/0xInfection/TIDoS-Framework) : Looks interesting. To test when python3 supported


Famous Wordlists : [SecLists](https://github.com/danielmiessler/SecLists)

With FFuF, the URL needs the keyword FUZZ:

`ffuf -c -w SecLists-master/Discovery/Web-Content/common.txt -u https://target:4242/FUZZ`

The parameter `-e ".conf",".ini",".yml",".yaml",".php",".html",".txt",".log",".png",".jpg",".js",".jsp",".asp",".aspx",".pdf","bak","bkp","old",".sh"` with with SecLists-master/Discovery/Web-Content/common.txt works quite well.

Same for wfuzz (`--hc 404` to hide fail) and gobuster:

`wfuzz --hc 404 -w SecLists-master/Discovery/Web-Content/common.txt http://docker.hackthebox.eu:32914/FUZZ`

`gobuster dir -w SecLists-master/Discovery/Web-Content/common.txt -u https://target:4242/FUZZ`

### SQL Injections

`sqlmap -u $URL`

### Encrypted values

Padding Oracle attack:
- [padbuster](https://github.com/GDSSecurity/PadBuster) : Perl script. The compilation of Crypt::SSLeay will likely fail, but is now useless. Comment the line.
- [poracle](https://github.com/iagox86/Poracle) : Need to write Ruby code

Python modules and clones also exist for that. To explore.

#### Padbuster

When specifying cookies, always put PHPSESSID first.

First, decrypt using `padBuster.pl URL EncryptedSample BlockSize` and `-cookies` option if EncryptedSample is in a cookie.
You will obtain the decrypted value, for example: `{"user"="SecT0uch"}`.

Once the pattern is known, we can generate or own Encrypted samples:
`padBuster.pl URL EncryptedSample BlockSize -plaintext '{"user"="root"}`